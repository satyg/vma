#!/usr/bin/env python3

import requests
import json
from datetime import datetime
import time

__author__          = 'Jonas Ingemarsson'
__copyright__       = 'Copyright 2022'
__version__         = '1.0.0'
__maintainer__      = 'Jonas Ingemarsson'
__email__           = 'ingemarsson.jonas@me.com'
__status__          = 'Production'
__last_change__     = '2022-12-16'
__dependencies__    = 'None'
__about__           = 'Telegrambot för att skicka ut VMA (Viktigt Meddelande till Allmänheten)'
                      # https://www.krisinformation.se/detta-gor-samhallet/vma-sa-varnas-allmanheten


def main():
    # Information om hur du skapar en bot på telegram finns här https://core.telegram.org/api
    _token       =  'SKRIV-IN-TOKEN-HÄR' # Fås efter du skapat din bot
    _id          =  'SKRIV-IN-ID-HÄR' # Fås efter du skapat din bot
    _telegram     = f'https://api.telegram.org/bot{_token}/sendMessage?chat_id={_id}&text='
    _url          = 'https://vmaapi.sr.se/api/v2/alerts'
    old_alerts   = []

    while True:
        today = datetime.now()
        tid = today.strftime('%H:%M:%S')
        for index in old_alerts:
            # Återställ klockan 00:00:00 varje dag för att förhindra
            # att samma meddelande skickas två gånger
            if index.endswith('00:00:00'):
                old_alerts = []
        # Kolla efter nytt VMA varje minut
        if tid.endswith('00'):
            response = requests.get(_url)
            if response.status_code == 200:
                response = json.loads(response.text)
                timestamp = response['timestamp'].replace('T', ' ')[:19]
                alert = response['alerts']
                # Om inte meddelandet är tomt...
                if len(alert) > 0 and timestamp not in old_alerts:
                    # Skicka meddelande till telegram och lägg till timestamp i lista med gamla VMA
                    requests.get(f'{_telegram}VIKTIGT MEDDELANDE TILL ALLMÄNHETEN\n{timestamp}\n{str(alert).replace("[", "").replace("]", "")}').json()
                    old_alerts.append(timestamp)     
        time.sleep(1)

if __name__ == '__main__':
    main()