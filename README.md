# Viktigt Meddelande till Allmänheten (VMA)
**NOTE**: *This information is in Swedish since it's regarding a Swedish public service*

[Myndigheten för samhällsskydd och beredskap](https://www.msb.se/) (MSB) skickar ibland ut [viktiga meddelanden till allmänheten](https://www.krisinformation.se/detta-gor-samhallet/vma-sa-varnas-allmanheten) (VMA). Dessa meddelanden kan hämtas via api från exempelvis [Sveriges Radio](https://vmaapi.sr.se/api/v2). Detta script kollar denna API varje minut efter nya meddelanden och skickar dessa till telegram via en bot som du själv skapat.

```python
_token       =  'SKRIV-IN-TOKEN-HÄR' # Fås efter du skapat din bot
_id          =  'SKRIV-IN-ID-HÄR' # Fås efter du skapat din bot
_telegram    = f'https://api.telegram.org/bot{_token}/sendMessage?chat_id={_id}&text='
```
Variablerna `_token` respektive `_id` kan du först sätta när du har skapat din bot. För att skapa en bot klickar du [HÄR](https://core.telegram.org/api) för att se instruktioner hur du ska göra.  

Lämpligast körs scriptet som en process i bakgrunden för att inte uppehålla dig i ditt vardagliga arbete på datorn. För att köra scriptet i bakgrunden så gör du sä här:  

**Linux & OS X**  
```
./vma.py &
```

**Windows**  
Döp om `vma.py` till `vma.pyw` så körs den i bakgrunden.  

För att öka automatiseringen ytterligare kan man lägga scriptet i autostart så att det alltid körs i bakgrunden när man startar datorn. (Googla hur du gör)

Nedan är kortare information om projektet
```python
__author__          = 'Jonas Ingemarsson'
__copyright__       = 'Copyright 2022'
__version__         = '1.0.0'
__maintainer__      = 'Jonas Ingemarsson'
__email__           = 'ingemarsson.jonas@me.com'
__status__          = 'Production'
__last_change__     = '2022-12-16'
__dependencies__    = 'None'
__about__           = 'Telegrambot för att skicka ut VMA (Viktigt Meddelande till Allmänheten)'
```